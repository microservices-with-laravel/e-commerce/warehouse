<?php

namespace App\Http\Controllers;

use App\Http\Requests\GetProductInventoryRequest;
use App\Http\Resources\ProductResource;
use App\Models\Product;
use App\Services\InventoryService;

class ProductInventoryController extends Controller
{
    public function __construct(
        private InventoryService $inventoryService
    ) {
    }

    public function index(GetProductInventoryRequest $request)
    {
        $inventories = $this->inventoryService->getProductsInventory(
            collect($request->getProductIds())
        );

        return [
            'data' => $inventories
        ];
    }

    public function get(Product $product)
    {
        return [
            'data' => new ProductResource($product)
        ];
    }
}
