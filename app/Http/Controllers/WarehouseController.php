<?php

namespace App\Http\Controllers;

use App\Http\Requests\StoreWarehouseRequest;
use App\Http\Resources\WarehouseResource;
use App\Repositories\WarehouseRepository;

class WarehouseController extends Controller
{
    public function __construct(private WarehouseRepository $warehouses)
    {
    }

    public function index()
    {
        return [
            'data' => WarehouseResource::collection($this->warehouses->getAll())
        ];
    }

    public function store(StoreWarehouseRequest $request)
    {
        return new WarehouseResource($this->warehouses->create($request->getName()));
    }
}
