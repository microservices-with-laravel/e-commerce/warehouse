<?php

namespace App\Http\Controllers;

use App\Http\Requests\StoreInventoryRequest;
use App\Repositories\ProductRepository;
use App\Repositories\WarehouseRepository;
use App\Services\InventoryService;
use Illuminate\Http\Response;

class InventoryController extends Controller
{
    public function __construct(
        private InventoryService $inventoryService,
        private ProductRepository $products,
        private WarehouseRepository $warehouses
    ) {
    }

    public function store(StoreInventoryRequest $request)
    {
        $inventoryContainer = $this->inventoryService->upsert(
            $this->products->getById($request->getProductId()),
            $this->warehouses->getById($request->getWarehouseId()),
            $request->getQuantity()
        );

        return response([
            'data' => $inventoryContainer
        ], Response::HTTP_CREATED);
    }
}
