<?php

namespace App\Services;

use Ecommerce\Common\Containers\Warehouse\InventoryContainer;
use Ecommerce\Common\Events\Warehouse\InventoryUpdatedEvent;
use Ecommerce\Common\Services\RedisService as BaseRedisService;

class RedisService extends BaseRedisService
{
    public function getServiceName(): string
    {
        return 'warehouse';
    }

    public function publishInventoryUpdated(InventoryContainer $inventoryContainer): void
    {
        $this->publish(new InventoryUpdatedEvent($inventoryContainer));
    }
}
