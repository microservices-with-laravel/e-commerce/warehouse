<?php

namespace App\Services;

use App\Models\Product;
use App\Repositories\ProductRepository;
use Ecommerce\Common\Containers\Product\ProductContainer;

class ProductService
{
    public function __construct(private ProductRepository $products)
    {
    }

    public function create(ProductContainer $productContainer): Product
    {
        return $this->products->create($productContainer->id, $productContainer->name);
    }
}
