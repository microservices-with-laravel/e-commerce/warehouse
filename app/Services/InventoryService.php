<?php

namespace App\Services;

use App\Models\Product;
use App\Models\Warehouse;
use App\Repositories\InventoryRepository;
use App\Repositories\ProductRepository;
use Ecommerce\Common\Containers\Order\OrderContainer;
use Ecommerce\Common\Containers\Warehouse\InventoryContainer;
use Illuminate\Support\Collection;
use Illuminate\Support\Facades\DB;

class InventoryService
{
    public function __construct(
        private RedisService $redisService,
        private InventoryRepository $inventories,
        private ProductRepository $products
    ) {
    }

    public function upsert(
        Product $product,
        Warehouse $warehouse,
        float $quantity
    ): InventoryContainer {
        return DB::transaction(function () use ($product, $warehouse, $quantity) {
            $this->inventories->upsert($product, $warehouse, $quantity);
            $totalQuantity = $this->inventories->getTotalQuantity($product);

            $inventoryContainer = $this->createContainer(
                $product,
                $totalQuantity
            );

            $this->redisService->publishInventoryUpdated($inventoryContainer);
            return $inventoryContainer;
        });
    }

    public function createContainer(Product $product, float $totalQuantity): InventoryContainer
    {
        return new InventoryContainer(
            $product->id,
            $totalQuantity
        );
    }

    /**
     * @param Collection<int> $productIds
     * @return Collection<InventoryContainer>
     */
    public function getProductsInventory(Collection $productIds): Collection
    {
        $products = $this->products->getByIds($productIds);
        $quantitiesByProductId = $this->inventories->getTotalQuantityByMultiple($products);

        return $quantitiesByProductId->map(fn (float $quantity, int $productId) => new InventoryContainer(
            $productId,
            $quantity
        ))->values();
    }

    public function decrease(OrderContainer $orderContainer): void
    {
        DB::transaction(function () use ($orderContainer) {
            $product = $this->products->getById($orderContainer->productId);
            $this->inventories->decrease($product, $orderContainer->quantity);

            $totalQuantity = $this->inventories->getTotalQuantity($product);
            $inventoryContainer = $this->createContainer($product, $totalQuantity);

            $this->redisService->publishInventoryUpdated($inventoryContainer);
        });
    }
}
