<?php

namespace App\Repositories;

use App\Models\Product;
use Illuminate\Support\Collection;

class ProductRepository
{
    public function create(int $id, string $name): Product
    {
        return Product::create(compact('id', 'name'));
    }

    public function getById(int $id): Product
    {
        return Product::findOrfail($id);
    }

    /**
     * @param Collection<int> $ids
     * @return Collection<Product>
     */
    public function getByIds(Collection $ids): Collection
    {
        return Product::whereIn('id', $ids)->get();
    }
}
