<?php

namespace App\Repositories;

use App\Exceptions\ProductInventoryExceededException;
use App\Models\Inventory;
use App\Models\Product;
use App\Models\Warehouse;
use Illuminate\Support\Collection;

class InventoryRepository
{
    public function upsert(Product $product, Warehouse $warehouse, float $quantity): Inventory
    {
        $inventory = Inventory::firstOrNew([
            'product_id' => $product->id,
            'warehouse_id' => $warehouse->id,
        ]);

        $inventory->quantity += $quantity;
        $inventory->save();

        return $inventory;
    }

    public function getTotalQuantity(Product $product): float
    {
        return Inventory::select('quantity')
            ->where('product_id', $product->id)
            ->sum('quantity');
    }

    /**
     * @param Collection<Product> $products
     * @return Collection<int, float>
     */
    public function getTotalQuantityByMultiple(Collection $products): Collection
    {
        return $products->mapWithKeys(fn (Product $product) => [
            $product->id => $this->getTotalQuantity($product)
        ]);
    }

    /**
     * @param Product $product
     * @param float $quantity
     * @throws ProductInventoryExceededException|\Throwable
     */
    public function decrease(Product $product, float $quantity): void
    {
        throw_if(
            $this->getTotalQuantity($product) < $quantity,
            new ProductInventoryExceededException(
                "There is not enough $product->name in inventory"
            )
        );

        $quantityLeft = $quantity;
        foreach ($product->inventories as $inventory) {
            /** @var Inventory $inventory */
            if ($inventory->quantity >= $quantityLeft) {
                $inventory->quantity -= $quantityLeft;
                $inventory->save();
                $this->deleteInventoryIfEmpty($inventory);
                break;
            }

            $quantityLeft -= $inventory->quantity;
            $inventory->delete();
        }
    }

    private function deleteInventoryIfEmpty(Inventory $inventory): void
    {
        if ($inventory->quantity === 0.0) {
            $inventory->delete();
        }
    }
}
