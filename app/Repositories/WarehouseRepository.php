<?php

namespace App\Repositories;

use App\Models\Warehouse;
use Illuminate\Support\Collection;

class WarehouseRepository
{
    public function getById(int $id): Warehouse
    {
        return Warehouse::findOrFail($id);
    }

    /**
     * @return Collection<Warehouse>
     */
    public function getAll(): Collection
    {
        return Warehouse::all();
    }

    public function create(string $name): Warehouse
    {
        return Warehouse::create([
            'name' => $name,
        ]);
    }
}
